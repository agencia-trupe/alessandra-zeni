<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefone'  => '(11) 2574-3706',
            'email'     => 'contato@alessandrazeni.com.br',
            'facebook'  => '/',
            'instagram' => '/'
        ]);
    }
}
