@extends('frontend.common.template')

@section('content')

    <div class="main projetos projetos-show">
        <div class="projetos-title">
            <div class="center">
                <h2>{{ $projeto->titulo }}</h2>
                <p>{{ $projeto->texto }}</p>
            </div>
        </div>

        <div class="slideshow">
            @foreach($projeto->imagens as $imagem)
            <div class="slide" style="background-image:url({{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }})"></div>
            @endforeach

            <div class="center">
                <div class="controls controls-prev"></div>
                <div class="controls controls-next"></div>
            </div>
        </div>

        <div class="voltar">
            <div class="center">
                <a href="{{ route('projetos.index', $projeto->categoria->slug) }}">Voltar</a>
            </div>
        </div>
    </div>

@endsection