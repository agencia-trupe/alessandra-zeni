@extends('frontend.common.template')

@section('content')

    <div class="main projetos projetos-index">
        <div class="center">
            <div class="projetos-categorias">
                @foreach($categorias as $cat)
                    @if(is_array($cat))
                        <div>
                            <a href="#" class="has-sub @if($categoria_selecionada->subcategoria === array_search($cat, $categorias)) open active @endif">{{ array_search($cat, $categorias) }}</a>
                            <div class="subcategorias">
                            @foreach($cat as $sub)
                                <a href="{{ route('projetos.index', $sub->slug) }}" @if($categoria_selecionada->id === $sub->id) class="active" @endif>{{ $sub->titulo }}</a>
                            @endforeach
                            </div>
                        </div>
                    @else
                        <a href="{{ route('projetos.index', $cat->slug) }}" @if($categoria_selecionada->id === $cat->id) class="active" @endif>{{ $cat->titulo }}</a>
                    @endif
                @endforeach

                <span class="categoria-selecionada">
                    @if($categoria_selecionada->subcategoria != null)
                        {{ $categoria_selecionada->subcategoria }}
                    @else
                        {{ $categoria_selecionada->titulo }}
                    @endif
                </span>
            </div>

            <div class="projetos-list">
                @foreach($projetos as $projeto)
                <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}">
                    <div class="image-wrapper">
                        <img src="{{ asset('assets/img/projetos/thumbs/'.$projeto->imagem) }}" alt="{{ $projeto->titulo }}">
                    </div>
                    <span>{{ $projeto->titulo }}</span>
                </a>
                @endforeach
            </div>

        </div>
    </div>

@endsection
