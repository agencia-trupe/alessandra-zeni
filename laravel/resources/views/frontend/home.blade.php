@extends('frontend.common.template')

@section('content')

    <div class="home-banner">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
            @endforeach
        </div>

        <div class="center">
            <div class="home-box">
                <h1>{{ config('site.name') }}</h1>

                <button id="home-nav-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>

                <nav id="nav-home">
                    @include('frontend.common.nav')
                    @if($contato->facebook)<a href="{{ $contato->facebook }}" class="social social-facebook">facebook</a>@endif
                    @if($contato->instagram)<a href="{{ $contato->instagram }}" class="social social-instagram">instagram</a>@endif
                </nav>
            </div>
        </div>
    </div>

@endsection
