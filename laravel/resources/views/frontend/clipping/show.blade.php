@extends('frontend.common.template')

@section('content')

    <div class="main clipping clipping-show">
        <div class="clipping-title">
            <div class="center">
                <h2>{{ $clipping->titulo }}</h2>
            </div>
        </div>

        <div class="slideshow">
            @foreach($clipping->imagens as $imagem)
            <div class="slide" style="background-image:url({{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }})"></div>
            @endforeach

            <div class="center">
                <div class="controls controls-prev"></div>
                <div class="controls controls-next"></div>
            </div>
        </div>

        <div class="voltar">
            <div class="center">
                <a href="{{ route('clipping') }}">Voltar</a>
            </div>
        </div>
    </div>

@endsection