@extends('frontend.common.template')

@section('content')

    <div class="main clipping clipping-index">
        <div class="center">
            @foreach($clipping as $clipping)
            <a href="{{ route('clipping', $clipping->slug) }}">
                <img src="{{ asset('assets/img/clipping/capa/'.$clipping->imagem) }}" alt="{{ $clipping->titulo }}">
            </a>
            @endforeach
        </div>
    </div>

@endsection