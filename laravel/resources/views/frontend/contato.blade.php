@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <div class="info">
                <p class="telefone">{{ $contato->telefone }}</p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
            </div>

            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="Nome" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
                <input type="text" name="assunto" id="assunto" placeholder="Assunto">
                <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                <input type="submit" value="Enviar">
                <div id="form-contato-response"></div>
            </form>
        </div>
    </div>

@endsection
