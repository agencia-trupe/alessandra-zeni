<a href="{{ route('perfil') }}" @if(Route::currentRouteName() == 'perfil') class="active" @endif>Perfil</a>
<a href="{{ route('projetos.index') }}" @if(str_is('projetos*', Route::currentRouteName())) class="active" @endif>Projetos</a>
<a href="{{ route('clipping') }}" @if(Route::currentRouteName() == 'clipping') class="active" @endif>Clipping</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>Contato</a>
