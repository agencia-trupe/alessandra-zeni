    <footer>
        <div class="center">
            <div class="col">
                <p class="telefone">{{ $contato->telefone }}</p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                @if($contato->facebook)<a href="{{ $contato->facebook }}" class="social social-facebook">facebook</a>@endif
                @if($contato->instagram)<a href="{{ $contato->instagram }}" class="social social-instagram">instagram</a>@endif
            </div>

            <div class="col col-orcamento">
                <div class="orcamento-wrapper">
                    <div class="orcamento">
                        <a href="{{ route('contato') }}">Orçamento</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="copyright">
        <div class="center">
            <p>© {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.</p>
            <p><a href="http://trupe.net" target="_blank">Criação de sites</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a></p>
        </div>
    </div>
