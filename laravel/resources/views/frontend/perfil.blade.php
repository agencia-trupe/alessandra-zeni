@extends('frontend.common.template')

@section('content')

    <div class="main perfil">
        <div class="center">
            <div class="texto">{!! $perfil->texto !!}</div>
            <div class="imagem">
                <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
