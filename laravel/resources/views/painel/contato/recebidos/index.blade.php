@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Contatos Recebidos</h2>
    </legend>

    @if(!count($contatosrecebidos))
    <div class="alert alert-warning" role="alert">Nenhuma mensagem recebida.</div>
    @else
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Data</th>
                <th>Assunto</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($contatosrecebidos as $contato)

            <tr class="tr-row @if(!$contato->lido) warning @endif">
                <td>{{ $contato->created_at }}</td>
                <td>@if($contato->assunto) {{ $contato->assunto }} @endif</td>
                <td>{{ $contato->nome }}</td>
                <td>{{ $contato->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.contato.recebidos.destroy', $contato->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.contato.recebidos.show', $contato->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $contatosrecebidos->render() !!}
    @endif

@stop
