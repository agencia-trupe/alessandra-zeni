@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem de Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/clipping/capa/'.$clipping->imagem) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 195px;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clipping.index') }}" class="btn btn-default btn-voltar">Voltar</a>
