(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.homeNavToggle = function() {
        var $handle = $('#home-nav-toggle'),
            $nav    = $('#nav-home');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.fadeToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
            speed: 1500
        });
    };

    App.showSubcategorias = function() {
        var $handle = $('.has-sub');
        if (!$handle.length) return;

        $handle.on('click touchstart', function(event) {
            event.preventDefault();

            var $subcategorias = $(this).next();

            if ($(this).hasClass('open')) {
                $subcategorias.fadeOut();
            } else {
                $subcategorias.fadeIn().css('display', 'inline-block');
            }

            $(this).toggleClass('open');
        });
    };

    App.slideshow = function() {
        var $wrapper = $('.slideshow');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            prev: '.controls-prev',
            next: '.controls-next'
        });

        $('.controls').click(function() {
            $wrapper.cycle('pause');
        });
    };

    App.resizeSlideshow = function() {
        var $wrapper = $('.slideshow');
        if (!$wrapper.length) return;

        var viewportHeight = $(window).height(),
            viewportWidth  = $(window).width(),
            wrapperHeight  = 920;

        if (viewportHeight <= 920) wrapperHeight = viewportHeight;
        if (viewportHeight / viewportWidth > 0.75) wrapperHeight = viewportHeight * 0.6;

        $wrapper.css('height', wrapperHeight);
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                assunto: $('#assunto').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.homeNavToggle();
        this.bannersHome();
        this.showSubcategorias();
        this.slideshow();
        this.resizeSlideshow();
        $(window).resize(this.resizeSlideshow);
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
