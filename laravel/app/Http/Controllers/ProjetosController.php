<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index(ProjetoCategoria $categoria_selecionada)
    {
        if (! $categoria_selecionada->exists) {
            $categoria_selecionada = ProjetoCategoria::ordenados()->first() ?: \App::abort('404');
        }

        $projetos = $categoria_selecionada->projetos()->get();

        return view('frontend.projetos.index', compact('categoria_selecionada', 'projetos'));
    }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        return view('frontend.projetos.show', compact('categoria', 'projeto'));
    }
}
