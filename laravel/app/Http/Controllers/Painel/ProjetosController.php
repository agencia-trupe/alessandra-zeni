<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetoRequest;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;
use App\Helpers\CropImage;

class ProjetosController extends Controller
{
    private $categorias;

    private $image_config = [
        'width'   => 525,
        'height'  => 400,
        'path'    => 'assets/img/projetos/thumbs/'
    ];

    public function __construct()
    {
        $this->categorias = ProjetoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (ProjetoCategoria::find($filtro)) {
            $projetos = Projeto::ordenados()->categoria($filtro)->get();
        } else {
            $projetos = Projeto::join('projetos_categorias as cat', 'cat.id', '=', 'projetos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('projetos.*')
                ->ordenados()->get();
        }

        return view('painel.projetos.index', compact('categorias', 'projetos', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.projetos.create', compact('categorias'));
    }

    public function store(ProjetoRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Projeto::create($input);
            return redirect()->route('painel.projetos.index')->with('success', 'Projeto adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar projeto: '.$e->getMessage()]);

        }
    }

    public function edit(Projeto $projeto)
    {
        $categorias = $this->categorias;

        return view('painel.projetos.edit', compact('categorias', 'projeto'));
    }

    public function update(ProjetoRequest $request, Projeto $projeto)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $projeto->update($input);
            return redirect()->route('painel.projetos.index')->with('success', 'Projeto alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar projeto: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $projeto)
    {
        try {

            $projeto->delete();
            return redirect()->route('painel.projetos.index')->with('success', 'Projeto excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir projeto: '.$e->getMessage()]);

        }
    }
}
