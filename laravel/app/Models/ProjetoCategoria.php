<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class ProjetoCategoria extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'slugcompleto',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'projetos_categorias';

    protected $guarded = ['id'];

    public function getSlugcompletoAttribute() {
        $slug = $this->titulo;
        if ($this->subcategoria) $slug = $this->subcategoria . '-' . $this->titulo;

        return $slug;
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function projetos()
    {
        return $this->hasMany('App\Models\Projeto', 'projetos_categoria_id')->ordenados();
    }
}
