<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer(['frontend.common.footer', 'frontend.home'], function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('frontend.projetos.*', function($view) {
            $categorias = \App\Models\ProjetoCategoria::ordenados()->get();
            foreach($categorias as $categoria) {
                if ($categoria->subcategoria) {
                    $cat[$categoria->subcategoria][] = $categoria;
                } else {
                    $cat[] = $categoria;
                }
            }

            $view->with('categorias', $cat);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
